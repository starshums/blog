FROM php:7.3-apache

LABEL maintainer="chems00rock@gmail.com"


#Set software source
ARG DEBIAN_URL=mirrors.ustc.edu.cn
ENV DEBIAN_URL ${DEBIAN_URL}
RUN sed -i "s/deb.debian.org/$DEBIAN_URL/g" /etc/apt/sources.list

ARG DEBIAN_SECURITY_URL=mirrors.ustc.edu.cn/debian-security
ENV DEBIAN_SECURITY_URL ${DEBIAN_SECURITY_URL}
RUN sed -i "s|security.debian.org/debian-security|$DEBIAN_SECURITY_URL|g" /etc/apt/sources.list

#Install software Cron
RUN set -eux \
        && apt-get update \
        && apt-get install -y --no-install-recommends cron \
        && apt-get autoremove \
        && apt-get autoclean \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get update
        
# #Installation expansion
# ENV PHP_EXTENSION \
#     pdo_mysql \
#     bcmath
# ENV PECL_EXTENSION \
#     redis
# RUN docker-php-ext-install $PHP_EXTENSION \
#     && pecl install $PECL_EXTENSION \
#     && docker-php-ext-enable $PECL_EXTENSION \ opcache \
#     && a2enmod rewrite

# Install docker
RUN apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common \
    && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
    && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
    && apt-get update \
    && apt-cache policy docker-ce \
    && apt-get install -y docker-ce

# Install zip
RUN apt-get install -y zip


# Install git
RUN apt-get install -y git


# Install composer
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN ls
RUN composer update
RUN chmod -R 777 storage && chmod -R 777 bootstrap/cache
RUN mv .env.example .env
RUN php artisan key:generate
    
#Access port
EXPOSE 80

#other

ARG APP_ENV=development
ENV APP_ENV ${APP_ENV}

RUN echo "ServerName localhost" >> /etc/apache2/sites-enabled/000-default.conf
COPY --chown=www-data:www-data . /var/www/html
COPY docker/000-default.conf /etc/apache2/sites-enabled/000-default.conf

WORKDIR /var/www/html